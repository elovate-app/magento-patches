# Elovate Magento patches
## Catalog API
Magento has an issue in the catalog API, which can result in unsetting product and category values on store view level. In order to prevent this, install the provided patches.

# Installation
```composer require elovate-app/magento-patches```

After installation, add the following to your composer.json file below the following line:

```"magento-force": "override"```

which results in:

```
"extra": {
    "magento-force": "override",
    "patches": {
        "magento/module-catalog": {
            "Magento 2 REST API patch for category API calls": "vendor/elovate-app/magento-patches/category-api/category_repository.patch",
            "Magento 2 REST API patch for product API calls": "vendor/elovate-app/magento-patches/product-api/product_repository.patch"
        }
    }
}
```

# Important notes
Magento 2.4.3 and above do not need the supplied Catalog API patches. In 2.4.3 and above this has already been resolved.
If you are running Magento on version 2.4.2, you only need to apply the product API patch:
```
"Magento 2 REST API patch for product API calls": "vendor/elovate-app/magento-patches/product-api/product_repository.patch"
```

For versions below 2.4.2, you need to install both patches:
```
"Magento 2 REST API patch for category API calls": "vendor/elovate-app/magento-patches/category-api/category_repository.patch",
"Magento 2 REST API patch for product API calls": "vendor/elovate-app/magento-patches/product-api/product_repository.patch"
```

# Support
If you are in need of assistance running these patches, send us an e-mail at <a href="mailto:support@elovate.io">support@elovate.io</a>. We can forward your request to our Magento development partner, HYPR. 